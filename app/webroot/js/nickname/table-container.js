/*
	Вспомогательные классы таблицы для быстрой инициализации и доступа к необходимым функциям
*/
object = function() { 
	this.body = null;
	this.equal = function(compared_object) {
		return this.body[0].isEqualNode(compared_object.body[0]);
	};
};
table = function() {};
table.row = function(init) {
	//	Наследование
	object.call(this);
	this.body = init.closest('tr');
	this.id = this.body.attr('row-id');
}

/*
	Функции работы пользователя с таблицей
*/
// $(document).on('click', '.table-container tbody tr', function(){
// 	var r = new table.row($(this));
// 	location.href = $.url('trainers/pupils/'+r.id);
// });