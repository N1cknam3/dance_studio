function login_success(data) {
    if (data) {
        $("#status_login").html(data);
        $("#status_login").show();
        animateElement1("#status_login", message_animation_effect);
    } else {
        $("#status_login").hide();
        location.reload();
    }
};

/*
    Функция проверки полей ввода формы
    При пустом data переходит по пути urlPathб,
    иначе показывает элемент elementSelector (сообщение об ошибке)
*/
function form_success(data, elementSelector, urlPath) {
    if (data) {
        $(elementSelector).html(data);
        $(elementSelector).show();
        animateElement1(elementSelector, message_animation_effect);
    } else {
        $(elementSelector).hide();
        location.href = $.url(urlPath);
    }
};