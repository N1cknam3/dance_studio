jQuery(document).ready(function($) {

    /* ===================================
        Функция для получения правильного
        адреса перехода AJAX-запросов
       =================================== */

    $.url = function(url) {
        var root = $('base').attr('href');
        if (url.charAt(0) === '/')
            return root+url.substr(1);
        else
            return root+url;
    };
    

});

var message_animation_effect = 'flash';
var input_animation_effect = 'shake';

function animateElement1(elementName, animationName) {
    $(elementName).removeClass(animationName + " animated");
        
        $(function() {
            setTimeout(function() {
                $(elementName).addClass(animationName + " animated");
            }, 0);
        });
        
    $(elementName).on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {$(elementName).removeClass(animationName + " animated")});
}