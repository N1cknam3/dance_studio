<div class="table-container">

	<h2>Список посещений направления <?php echo @$direction_name; ?></h2>
	
	<?php
		if (empty($table_data)) {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Нет посещений");
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list/'.$direction_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Новое посещение',													
					array('action' => 'lesson_add/'.$direction_id),
					array(
						'escape' => false,
						'class' => 'btn btn-primary button responsive-button green'
				));
			echo '</div>';
		} else {
			echo $this->Widgets->table(
				array('Дата', 'Действия'),
				$table_data,
				array(
					'edit' => array(
						'action' => 'lesson_edit/'.$direction_id,
						'title' => 'Редактировать посещение'
					),
					'delete' => array(
						'action' => 'lesson_delete/'.$direction_id,
						'title' => 'Удалить посещение'
					)
				)
			);

			echo '<div class="row group-button-container no-margin">';
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list'), array('class' => 'btn btn-primary button button_add lighter group-button col-md-3', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Новое посещение',													
					array('action' => 'lesson_add/'.$direction_id),
					array(
						'escape' => false,
						'class' => 'button_add text-center col-md-6'
				));
				echo $this->Html->link(
					'<i class="fa fa-line-chart"></i> График',													
					array('action' => 'direction_chart/'.$direction_id),
					array(
						'escape' => false,
						'class' => 'button_add lighter text-center col-md-3'
				));
			echo '</div>';
		}
	?>

</div>