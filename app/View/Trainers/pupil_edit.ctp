<div class="table-container text-center">

	<h2>Данные ученика</h2>
	
	<?php

		$ajax_data = $this->Js->get('#PupilForm')->serializeForm(
	                                                array(
	                                                'isForm' => true,
	                                                'inline' => true)
	                                            );
	 
	    // Submit the serialize data on submit click
	    $this->Js->get('#PupilForm')->event(
         	'submit',
         	$this->Js->request(
	            array(
		            'controller' => 'trainers',
	            	'action' => 'ajax_pupil_data_check'
	            ),
	            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $ajax_data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST',
                    'success' => 'form_success(data, ".status-form", "/trainers/pupil_list/'.$direction_id.'")'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
	        )
        );

		echo $this->Form->create(
			'Pupil',
			array(
				'url' => $this->Html->url(array('controller' => 'trainers', 'action' => 'pupil_add')),
				'default' => true,
				'id' => 'PupilForm'
			)
		);
		
		echo $this->Widgets->Form(

			array(
				array(
					'type' => 'hidden',
					'name' => 'id',
					'value' => @$pupil['id']
				),

				array(
					'type' => 'select',
					'name' => 'direction_id',
					'label' => 'Направление',
					'options' => @$pupil['directions'],
					'selected' => @$pupil['direction_id']
				),

				array(
					'type' => 'text',
					'name' => 'name',
					'label' => 'Имя',
					'value' => @$pupil['name'],
					'placeholder' => 'Введите имя'
				)
			)

		);
		
		echo '<div class="status-form"></div>';

		echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'pupil_list/'.$direction_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
		echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button responsive-button green', 'div' => false, 'style' => 'margin-right: 25px;'));
		echo $this->Form->end();
		echo $this->Js->writeBuffer();
	?>

</div>