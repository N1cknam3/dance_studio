<div class="table-container">

	<h2>Список танцевальных направлений</h2>
	
	<?php
		if (empty($directions)) {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Вам не было задано ни одного направления подготовки!");
				// echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list'), array('class' => 'btn btn-primary button responsive-button one', 'div' => false, 'escape' => false));
			echo '</div>';
		} else
			echo $this->Widgets->table(
				array('Направление', 'Действия'),
				$directions,
				array(
					'table' => array(
						'action' => 'lesson_list',
						'title' => 'Открыть список посещений'
					),
					'list' => array(
						'action' => 'pupil_list',
						'title' => 'Открыть список учеников'
					),
				)
			);
	?>

</div>