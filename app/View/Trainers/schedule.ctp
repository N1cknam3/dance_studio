<div class="table-container">

	<?php
		echo $this->element('schedule');
	?>

</div>
<!-- ___Post Meta___ -->
<div class="post-meta">
	<a href="#0" class="share-icon">
		<i class="fa fa-share-alt"></i>
		<span>25 Share</span>
	</a>
	<a href="#0" class="category-icon pull-right">
		<span>Travel</span>
		<i class="fa fa-plane"></i>
	</a>
</div>