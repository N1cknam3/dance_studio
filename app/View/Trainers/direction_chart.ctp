<div class="table-container">

	<h2>График посещений направления <?php echo @$direction_name; ?></h2>
	
	<div class="my-no-data">

		<?php echo $this->element('direction_chart'); ?>

		<?php
			echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'lesson_list/'.$direction_id), array('class' => 'btn btn-primary button responsive-button one', 'div' => false, 'escape' => false));
		?>

	</div>
</div>