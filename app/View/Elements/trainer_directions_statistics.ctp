<?php
	//	Массив будет содержать контейнеры всех графиков
	$chart_containers = array();
?>

<script type="text/javascript">
	<?php
		$count = 1;
		foreach ($charts_data as $chart_data) {
			switch ($chart_data['type']) {
				case 'pie':
					echo $this->Widgets->GetPieChart($chart_data, $count, $chart_containers);	//	В $chart_containers помещается необходимый контейнер
					break;
				case 'single_text':
					echo $this->Widgets->GetInfo($chart_data, $count, $chart_containers);
					break;
				default:
					continue;
			}
			$count++;
		}
	?>
</script>

<div class="row">
	<?php
		foreach ($chart_containers as $container) {
			echo $container;
		}
	?>
</div>