<?php
	echo $this->Widgets->Form(

		array(

			array(
				'type' => 'static',
				'label' => 'Кол-во занятий',
				'value' => @$form_data['lessons_count'],
				'default' => '0'
			),

			array(
				'type' => 'static',
				'label' => 'Кол-во посещений',
				'value' => @$form_data['pupils_count'],
				'default' => '0'
			),

			array(
				'type' => 'text',
				'name' => 'n',
				'label' => false,
				'value' => @$form_data['pupils_count'],
				'default' => '0',
				'style' => 'display: none;'
			),

			array(
				'type' => 'static',
				'label' => 'Цена занятия',
				'value' => @$form_data['cost_lesson'],
				'default' => '-'
			),

			array(
				'type' => 'static',
				'label' => 'ЗП преподавателя за ученика',
				'value' => @$form_data['cost_trainer'],
				'default' => '-'
			),

			array(
				'type' => 'static',
				'label' => 'Аренда зала',
				'value' => @$form_data['cost_rent'],
				'default' => '-'
			),

			array(
				'type' => 'static',
				'label' => 'Плата за электричество',
				'value' => @$form_data['cost_electricity'],
				'default' => '-'
			),

			array(
				'type' => 'static',
				'label' => 'Прибыль:',
				'value' => @$form_data['income_money_html'],
				'default' => '-'
			)
		)
	);
?>