<div id="loaded_schedule">
	<h1>Расписание занятий</h1>
	<?php
		if (isset($schedule) && $schedule) {
			echo $this->Html->image("../files/schedules/".$schedule, array(
			    'style' => 'width: 100%;'
			));
		} else {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Расписание ещё не готово");
			echo '</div>';
		}
	?>
</div>