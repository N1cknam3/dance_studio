<div id="loaded_schedule">
	<h1>Наши видео</h1>
	<?php
		if (isset($videos) && $videos && !empty($videos)) {
			echo '<div class="row">';
			foreach ($videos as $video) {
				$video = $video['Video'];
				echo $this->Widgets->Video($video, $video_delete);
			}
			echo '</div>';
		} else {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Подождите, скоро здесь появится много крутых роликов с нашим участием!");
			echo '</div>';
		}
	?>
</div>