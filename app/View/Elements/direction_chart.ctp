<script type="text/javascript">

	<?php

		if ($chart_data && (count($chart_data) > 0)) {
			echo "var chartData1 = " . json_encode($chart_data) . ";";
		}

	?>

	var chart2 = AmCharts.makeChart("chartdiv2", {
	  "type": "serial",
	  "theme": "light",
	  "legend": {
	    "useGraphSettings": true
	  },
	  "dataProvider": chartData1,
	  "valueAxes": [{
	    "id": "v1",
	    "axisColor": "#FF6600",
	    "axisThickness": 2,
	    "gridAlpha": 0,
	    "axisAlpha": 1,
	    "position": "left"
	  }],
	  "graphs": [{
	    "valueAxis": "v1",
	    "lineColor": "#FF6600",
	    "bullet": "round",
	    "bulletBorderThickness": 1,
	    "hideBulletsCount": 30,
	    "title": "Посещения",
	    "valueField": "count",
	    "fillAlphas": 0
	  }],
	  "chartScrollbar": {},
	  "chartCursor": {
	    "cursorPosition": "middle",
	    "valueLineBalloonEnabled": true,
	    "valueLineEnabled": true
	  },
	  "categoryField": "date",
	  "categoryAxis": {
	    "parseDates": false,
	    "axisColor": "#DADADA",
	    "minorGridEnabled": true
	  }
	});

	chart2.addListener("init", zoomChartInit);

	chart2.addListener("dataUpdated", zoomChart);
	zoomChart();

	//	Определенное выделение в начале
	function zoomChartInit() {
		var start = <?php echo count($chart_data) - 1; ?> - 4;
		var end = <?php echo count($chart_data) - 1; ?>;
		chart2.zoomToIndexes(start, end);
	}

	function zoomChart() {
	  chart2.zoomToIndexes(chart2.dataProvider.length - 20, chart2.dataProvider.length - 1);
	}

</script>

<?php if ($chart_data && (count($chart_data) > 0)): ?>
	<div id="chartdiv2" style="width: 100%; height: 500px;"></div>
<?php else : ?>
	<?php echo $this->Widgets->no_data("Нет данных для отображения"); ?>
<?php endif; ?>