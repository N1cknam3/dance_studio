<?php
class WidgetsHelper extends AppHelper {
	
	public $helpers = array('Html', 'Form', 'Js');

	public function table($names, $data, $actions = null) {
		$answer = '';

		$answer .= '<table class="table table-bordered table-hover no-margin">';
			$answer .= '<thead>';

				$answer .= $this->table_row_header("#", $names);

			$answer .= '</thead>';
			$answer .= '<tbody>';
				
				$count = 1;
				foreach ($data as $d) {
					$answer .= $this->table_row($count, $d, $actions);
					$count++;
				}

			$answer .= '</tbody>';
		$answer .= '</table>';

		return $answer;
	}

	public function table_row_header($num, $data) {
		$answer = '';

		$answer .= '<tr>';

			$answer .= '<th class="numerics">'.$num.'</th>';

			foreach ($data as $d) {
				$answer .= '<th>'.$d.'</th>';
			}
			
		$answer .= '</tr>';

		return $answer;
	}
	
	public function table_row($num, $data, $actions = null) {
		$answer = '';

		$id = null;
		if (isset($data['id'])) {
			$id = $data['id'];
			unset($data['id']);
		}

		$answer .= '<tr row-id="'.$id.'">';

			$answer .= '<th class="numerics">'.$num.'</th>';

			foreach ($data as $d) {
				$answer .= '<td>'.$d.'</td>';
			}

			if ($actions) {
				$answer .= '<td class="button-area">'.$this->col_actions($actions, $id).'</td>';
			}
			
		$answer .= '</tr>';

		return $answer;
	}

	public function no_data($message) {
		$answer = '';

		$answer .= '<div class="my-no-data">';
			$answer .= '<span class="big-text unactive-text">' . $message . '</span>';
		$answer .= '</div>';

		return $answer;
	}

	public function col_actions($actions, $id) {
		$answer = '';

		$count = count($actions);

		if ($count > 0) {

			$col_width = floor(12.0 / $count);

			// $answer .= '<div>';

			foreach ($actions as $k => $data) {
				switch ($k) {
					case 'show':
						$answer .= $this->row_action_show($data, $id, $col_width);
						break;

					case 'edit':
						$answer .= $this->row_action_edit($data, $id, $col_width);
						break;

					case 'delete':
						$answer .= $this->row_action_delete($data, $id, $col_width);
						break;

					case 'list':
						$answer .= $this->row_action_list($data, $id, $col_width);
						break;

					case 'table':
						$answer .= $this->row_action_table($data, $id, $col_width);
						break;
					
					default:
						break;
				}
			}

			// $answer .= '</div>';
		}

		return $answer;
	}

	public function row_action_show($data, $id, $col_width) {
		$answer = '';

		$action = (isset($data['action'])) ? $data['action'] : "#";
		$title = (isset($data['title'])) ? $data['title'] : "Открыть";

		$answer .= $this->Html->link(
			__(''),
			array('action' => $action.'/'.$id),
			array('escape' => false, 'title' => $title, 'class' => 'button col-md-'.$col_width.' fa fa-search')
		);

		return $answer;
	}

	public function row_action_edit($data, $id, $col_width) {
		$answer = '';

		$action = (isset($data['action'])) ? $data['action'] : "#";
		$title = (isset($data['title'])) ? $data['title'] : "Редактирование";

		$answer .= $this->Html->link(
			__(''),
			array('action' => $action.'/'.$id),
			array('escape' => false, 'title' => $title, 'class' => 'button col-md-'.$col_width.' fa fa-pencil')
		);

		return $answer;
	}

	public function row_action_delete($data, $id, $col_width) {
		$answer = '';

		$action = (isset($data['action'])) ? $data['action'] : "#";
		$title = (isset($data['title'])) ? $data['title'] : "Удаление";
		$confirm = (isset($data['confirm'])) ? $data['confirm'] : "Вы действительно хотите удалить?";

		$answer .= $this->Form->postLink(
			__(''),
			array('action' => $action.'/'.$id),
			array(
				'confirm' => $confirm,
				'escape' => false,
				'title' => $title,
				'class' => 'button col-md-'.$col_width.' fa fa-trash-o'
			)
		);

		return $answer;
	}

	public function row_action_list($data, $id, $col_width) {
		$answer = '';

		$action = (isset($data['action'])) ? $data['action'] : "#";
		$title = (isset($data['title'])) ? $data['title'] : "Открыть список";

		$answer .= $this->Html->link(
			__(''),
			array('action' => $action.'/'.$id),
			array('escape' => false, 'title' => $title, 'class' => 'button col-md-'.$col_width.' fa fa-list')
		);

		return $answer;
	}

	public function row_action_table($data, $id, $col_width) {
		$answer = '';

		$action = (isset($data['action'])) ? $data['action'] : "#";
		$title = (isset($data['title'])) ? $data['title'] : "Открыть таблицу";

		$answer .= $this->Html->link(
			__(''),
			array('action' => $action.'/'.$id),
			array('escape' => false, 'title' => $title, 'class' => 'button col-md-'.$col_width.' fa fa-table')
		);

		return $answer;
	}

	public function Form($input_data = array()) {
		$answer = '';

		$answer .= '<div class="inputs-left">';

			foreach ($input_data as $k => $a) {
				if (!isset($a['type']))
					continue;
				switch ($a['type']) {
					case 'hidden':
						$answer .= $this->Form->input(@$a['name'], array(
							'type' => 'hidden',
							'class' => 'form-control',
							'value' => @$a['value']
						));
						break;

					case 'text':
						$answer .= $this->Form->input(@$a['name'], array(
							'type' => 'text',
							'class' => 'form-control',
							'label' => @$a['label'],
							'value' => @$a['value'],
							'default' => @$a['default'],
							'placeholder' => @$a['placeholder'],
							'style' => @$a['style'],
							'div' => 'input_container'
						));
						break;

					case 'select':
						$answer .= $this->Form->input(@$a['name'], array(
							'type' => 'select',
							'class' => 'form-control',
							'label' => @$a['label'],
							'options' => @$a['options'],
							'selected' => @$a['selected'],
							'div' => 'input_container'
						));
						break;

					case 'checkbox':
						$answer .= $this->Form->input(@$a['name'], array(
							'label' => @$a['label'],
							'type' => 'select',
							'multiple' => 'checkbox',
							'format' => array('before', 'label', 'input', 'between', 'after', 'error'),
							'options' => @$a['options'],
							'selected' => @$a['selected'],
							'div' => 'input_container'
						));
						break;

					case 'date':
						$others = isset($a['others']) ? $a['others'] : array();

						$answer .= $this->Form->input(@$a['name'], array(
							'type' => 'date',
							'class' => 'form-control input-date',
							'label' => @$a['label'],
							'value' => @$a['value'],
						    'dateFormat' => isset($a['dateFormat']) ? $a['dateFormat'] : 'DMY',
						    'minYear' => @$a['minYear'],
						    'maxYear' => @$a['maxYear'],
							'div' => 'input_container'
						) + $others);
						break;

					case 'static':
						$answer .=
						'<div class="input_container">
						<label>'.@$a['label'].'</label>
						<span>' . ($a['value'] ? @$a['value'] : @$a['default']) . '</span></div>';
						break;
					
					default:
						break;	
				}
			}

			$answer .= '<div class="status-message" style="margin-left: 30%;">'.@$error_text.'</div>';
		
		$answer .= '</div>';

		return $answer;
	}

	public function Video($video, $controls = false) {
		$answer = '';

		$answer .= '<div class="col-md-6" style="height: 350px; margin-bottom: 20px;">';
			$answer .= '<div class="video-container" style="height: 315px;">';
			$answer .= $video['html'];
			$answer .= '</div>';
			if ($controls) {
				$answer .= $this->Html->link(
					'<i class="fa fa-times"></i> Удалить',													
					array('controller' => 'admins', 'action' => 'video_delete/' . $video['id']),
					array(
						'escape' => false,
						'class' => 'btn btn-primary button responsive-button red one',
						'style' => 'width: 100%; margin: 0;'
				));
			}
		$answer .= '</div>';

		return $answer;
	}
	
	public function GetChartSettings($graphsNumberAndColors, $number) {
		$answer = '';

		//-----------------
		//НАСТРОЙКИ ГРАФИКА
		$chartName = "chart".$number;	//имя
		$chartDataName = "chartData".$number;	//имя источника данных

		$answer .= $chartName.' = new AmCharts.AmSerialChart();';	//создание
		$answer .= $chartName.'.dataProvider = '.$chartDataName.";";	//назначение источника данных
		$answer .= $chartName.'.categoryField = "anket_date";
		';	//назначение имен горизонтальной оси определенному полю
		//$answer .= $chartName.'.startDuration = 0.35;';	//скорость анимации появления
		//$answer .= $chartName.'.startEffect = ">";';	//анимация появления

		//---------------------------------
		//НАСТРОЙКИ ВЕРТИКАЛЬНОЙ ОСИ ОЦЕНОК
		$valAxisName = "yAxis".$number;	//имя

		$answer .= 'var '.$valAxisName.' = new AmCharts.ValueAxis();';	//создание
		$answer .= $valAxisName.'.position = "left";';	//позиция оси слева
		$answer .= $valAxisName.'.autoGridCount = false;';	//назначаем фиксированные значения
		$answer .= $valAxisName.'.gridCount = 6;';	//назначаем кол-во горизонтальных линий
		$answer .= $valAxisName.'.minimum = 0;';	//минимальное значение
		$answer .= $valAxisName.'.maximum = 6;';	//максимальное значение
		$answer .= $valAxisName.'.title = "Оценка";';	//название оси
		$answer .= $valAxisName.'.showFirstLabel = false;';	//убираем первое значение
		$answer .= $valAxisName.'.showLastLabel = false;';	//убираем последнее значение

		$answer .= $chartName.'.addValueAxis('.$valAxisName.');
		';	//назначение оси графику

		//----------------------------------------------
		//НАСТРОЙКИ ГОРИЗОНТАЛЬНОЙ ОСИ УЧЕБНЫХ ЧЕТВЕРТЕЙ
		$categoryAxisName = 'categoryAxis'.$number;	//имя

		$answer .= 'var '.$categoryAxisName.' = '.$chartName.'.categoryAxis;';	//создание
		// $answer .= '	'.$categoryAxisName.'.labelRotation = 45;';	//поворот меток
		$answer .= $categoryAxisName.'.autoGridCount  = false;';	//назначаем фиксированные значения
		$answer .= $categoryAxisName.'.gridCount = '.$chartDataName.'.length;';  	//назначаем кол-во вертикальных линий               
		$answer .= $categoryAxisName.'.gridPosition = "middle";';	//положение горизонтальных линий относительно меток
		//$answer .= $categoryAxisName.'.title = chartDataName;';	//положение горизонтальных линий относительно меток

		//---------------------
		//НАСТРОЙКИ SCROLLBAR'а
		// $scrollName = 'scroll'.$number;	//имя

		// $answer .= $scrollName.' = new AmCharts.ChartScrollbar();';	//создание
		// $answer .= $scrollName.'.dragIcon = "dragIcon";';	//ползунок

		// $answer .= $chartName.'.addChartScrollbar('.$scrollName.');';	//назначение scrollbar'а графику

		$tmp_count = 1;
		foreach ($graphsNumberAndColors as $key => $value) {
			//	$value => {["number"], ["color"]}

			//---------------
			//НАСТРОЙКИ ГРАФА
			$graphName = 'graph'.$value["number"];	//имя

			$answer .= $graphName.' = new AmCharts.AmGraph();';	//создание
			$answer .= $graphName.'.valueField = "'.$key.'";';	//назначение поля источника данных
			$answer .= $graphName.'.balloonText = "[[category]]: оценка=<b>[[value]]</b>";';	//назачение формата текста при наведении
			$answer .= $graphName.'.type = "line";';	//тип графа
			$answer .= $graphName.'.bullet = "round";';	//тип точек
			$answer .= $graphName.'.lineColor = "'.$value["color"].'";';	//цвет графа

			if ($tmp_count > 1) {
				$answer .= $graphName.'.hidden = true;';	//скрываем все графы, кроме первого
			};
			$tmp_count++;

			$answer .= $chartName.'.addGraph('.$graphName.');
			';	//назначение графа графику

		}

		//-------------------
		//НАСТРОЙКИ ОТРИСОВКИ
		$chartDivID = $chartName.'div';	//имя блока отрисовки

		$answer .= $chartName.'.write("'.$chartDivID.'");
		';	//назначение блока отрисовки

		return $answer;
	}

	public function GetPieChart($data, $num, &$chart_containers) {
		$answer = '';

		//	Если в график прийдут только нулевые данные, то мы не строим график
		$exists_chart = false;
		foreach ($data['data'] as $value) {
			if ($value['value'] !== 0) {
				$exists_chart = true;
				break;
			}
		}

		if ($exists_chart) {
			$answer .= '
			setTimeout(function(){
				var chart = AmCharts.makeChart("chartdiv'.$num.'", {
				  "type": "pie",
				  "theme": "light",
				  "startEffect" : "<",
				  "sequencedAnimation": false,
				  "startAlpha": 0,
				  "startDuration": 1,
				  "startRadius": "100%",
				  "dataProvider": ' . json_encode($data['data']) . ',
				  "titleField": "title",
				  "valueField": "value",
				  "labelRadius": 5,
				  "radius": "25%",
				  "innerRadius": "50%",
				  "labelText": "[[title]]",
				  "export": {
				    "enabled": true
				  }
				} );
			}, ' . ($num - 1) * 300 . ');';

			array_push($chart_containers, '<div class="col-md-6"><div id="chartdiv'.$num.'" style="height: 300px; border: 1px dashed #C3C3C3; border-radius: 8px;"></div><text class="clearfix">'.$data['name'].'</text></div>');
		} else {
			array_push($chart_containers, '<div class="col-md-6"><div class="my-no-data" style="height: 300px; padding-top: 140px; border: 1px dashed #C3C3C3; border-radius: 8px;">'.$this->no_data('Нет данных').'</div><text class="clearfix">'.$data['name'].'</text></div>');
		}

		return $answer;
	}

	public function GetInfo($data, $num, &$chart_containers) {
		$answer = '';

		array_push($chart_containers, '<div class="col-md-6"><div class="my-no-data" style="height: 300px; padding-top: 140px; border: 1px dashed #C3C3C3; border-radius: 8px;"><div style="font-size: 34px;">'.$data['value'].'</div></div><text class="clearfix">'.$data['name'].'</text></div>');

		return $answer;
	}

	public function GetLoadingAnimationHtml() {
		return '<div style="height: 235px; margin-bottom: 30px; padding-top: 100px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>';
	}
	
}
?>