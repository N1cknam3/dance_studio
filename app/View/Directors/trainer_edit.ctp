<div class="table-container text-center">

	<h2>Данные преподавателя</h2>
	
	<?php

		$ajax_data = $this->Js->get('#UserForm')->serializeForm(
	                                                array(
	                                                'isForm' => true,
	                                                'inline' => true)
	                                            );
	 
	    // Submit the serialize data on submit click
	    $this->Js->get('#UserForm')->event(
         	'submit',
         	$this->Js->request(
	            array(
		            'controller' => 'directors',
	            	'action' => 'ajax_trainer_data_check'
	            ),
	            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $ajax_data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST',
                    'success' => 'form_success(data, ".status-form", "/directors/trainers_list")'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
	        )
        );

		echo $this->Form->create(
			'User',
			array(
				'url' => $this->Html->url(array('controller' => 'directors', 'action' => 'trainer_add')),
				'default' => true,
				'id' => 'UserForm'
			)
		);
		
		echo $this->Widgets->Form(

			array(
				array(
					'type' => 'hidden',
					'name' => 'id',
					'value' => @$trainer['id']
				),

				array(
					'type' => 'text',
					'name' => 'name',
					'label' => 'Имя',
					'value' => @$trainer['name'],
					'placeholder' => 'Введите имя'
				),

				array(
					'type' => 'text',
					'name' => 'email',
					'label' => 'Email',
					'value' => @$trainer['email'],
					'placeholder' => 'Введите email'
				),

				array(
					'type' => 'text',
					'name' => 'password',
					'label' => 'Пароль',
					'value' => @$trainer['password'],
					'placeholder' => 'Введите пароль'
				)
			)

		);
		
		echo '<div class="status-form"></div>';

		echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'trainers_list'), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
		echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button responsive-button green', 'div' => false, 'style' => 'margin-right: 25px;'));
		echo $this->Form->end();
		echo $this->Js->writeBuffer();
	?>

</div>