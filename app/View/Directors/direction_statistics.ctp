<script>
	function get_ajax_direction_statistics_form() {
		$.ajax({
			type: "POST",
			
			url: <?php echo "\"directors/get_direction_statistics_form/" . $direction_id . "\""; ?>,
			
			data: {
				month: $(document).find("#Month").val(),
				year: $(document).find("#Year").val()
			},

			beforeSend: function() {
				var html_string = <?php echo "'" . $this->Widgets->GetLoadingAnimationHtml() . "'"; ?>;
				$("#ajax_direction_statistics_form").html(html_string);
			},
			
			success: function(data){
				$("#ajax_direction_statistics_form").html(data)
			},
	        error : function(data) {
	           alert("Ошибка получения данных по AJAX");
	        }
		});
	}
</script>

<div class="table-container">

	<h2>Статистика направления <?php echo @$direction_name; ?></h2>
	<h4>Преподаватель: <?php echo $trainer_name; ?></h4>
	
	<div class="my-no-data">

		<?php

		$ajax_data = $this->Js->get('#StatisticsForm')->serializeForm(
	                                                array(
	                                                'isForm' => true,
	                                                'inline' => true)
	                                            );
	 
	    // Submit the serialize data on submit click
	    $this->Js->get('#StatisticsForm')->event(
         	'submit',
         	$this->Js->request(
	            array(
		            'controller' => 'directors',
	            	'action' => 'ajax_statistics_data_check'
	            ),
	            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $ajax_data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                    ,'success' => 'form_success(data, ".status-form", "")'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
	        )
        );

		echo $this->Form->create(
			false,
			array(
				'url' => $this->Html->url(array('controller' => 'directors', 'action' => 'lesson_add')),
				'default' => true,
				'id' => 'StatisticsForm'
			)
		);

		echo $this->Widgets->Form(
			array(
				array(
					'type' => 'date',
					'label' => 'Период отчета',
					'dateFormat' => 'MY',
					'value' => @$form_data['date']['value'],
					'minYear' => @$form_data['date']['minYear'],
					'maxYear' => @$form_data['date']['maxYear'],
					'others' => array(
						'onChange' => "get_ajax_direction_statistics_form()"
					)
				)
			)
		);
		
		echo '<div id="ajax_direction_statistics_form">';
			//	Динамически меняющаяся часть
			echo $this->element('direction_statistics_form');
		echo '</div>';

		// echo $this->Widgets->Form(
		// 	array(
		// 		array(
		// 			'type' => 'text',
		// 			'name' => 'k',
		// 			'label' => 'Цена занятия',
		// 			'value' => @$form_data['lesson_cost'],
		// 			'default' => '200'
		// 		),

		// 		array(
		// 			'type' => 'text',
		// 			'name' => 'z',
		// 			'label' => 'ЗП преподавателя за ученика',
		// 			'value' => @$form_data['trainer_cost'],
		// 			'default' => '100'
		// 		),

		// 		array(
		// 			'type' => 'text',
		// 			'name' => 'A',
		// 			'label' => 'Аренда зала',
		// 			'value' => @$form_data['rent'],
		// 			'default' => '200'
		// 		),

		// 		array(
		// 			'type' => 'text',
		// 			'name' => 'Q',
		// 			'label' => 'Плата за электричество',
		// 			'value' => @$form_data['electricity'],
		// 			'default' => '2000'
		// 		)
		// 	)
		// );

		echo '<div class="status-form"></div>';

		// echo $this->Form->submit('Посчитать', array('class' => 'btn btn-primary button responsive-button one green', 'div' => false));
		echo $this->Form->end();
		echo $this->Js->writeBuffer();
	?>

		<?php
			$action = isset($back_button_action) ? $back_button_action : 'lesson_list/'.$trainer_id."/".$direction_id;
			echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => $action), array('class' => 'btn btn-primary button responsive-button one', 'div' => false, 'escape' => false));
		?>

	</div>
</div>