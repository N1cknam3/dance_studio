<div class="table-container">

	<h2>Список преподавателей</h2>
	
	<?php
		if (empty($data)) {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Нет преподавателей");
				// echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list/'.$direction_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить преподавателя',													
					array('action' => 'trainer_add'),
					array(
						'escape' => false,
						'class' => 'btn btn-primary button responsive-button green'
				));
			echo '</div>';
		} else {
			echo $this->Widgets->table(
				array('Имя', 'Действия'),
				$data,
				array(
					'edit' => array(
						'action' => 'trainer_edit',
						'title' => 'Редактировать данные преподавателя'
					),
					'list' => array(
						'action' => 'direction_list',
						'title' => 'Открыть список направлений преподавателя'
					),
					'delete' => array(
						'action' => 'trainer_delete',
						'title' => 'Удалить преподавателя'
					)
				)
			);

			echo '<div class="row group-button-container no-margin">';
				// echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list'), array('class' => 'btn btn-primary button button_add group-button col-md-3', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить преподавателя',													
					array('action' => 'trainer_add'),
					array(
						'escape' => false,
						'class' => 'button_add text-center col-md-12'
				));
			echo '</div>';
		}
	?>

</div>