<div class="table-container text-center">

	<h2>Новое танцевальное направление</h2>
	
	<?php

		$ajax_data = $this->Js->get('#DirectionForm')->serializeForm(
	                                                array(
	                                                'isForm' => true,
	                                                'inline' => true)
	                                            );
	 
	    // Submit the serialize data on submit click
	    $this->Js->get('#DirectionForm')->event(
         	'submit',
         	$this->Js->request(
	            array(
		            'controller' => 'directors',
	            	'action' => 'ajax_direction_data_check'
	            ),
	            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $ajax_data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST',
                    'success' => 'form_success(data, ".status-form", "/directors/direction_list/'.$trainer_id.'")'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
	        )
        );

		echo $this->Form->create(
			'Direction',
			array(
				'url' => $this->Html->url(array('controller' => 'directors', 'action' => 'direction_add')),
				'default' => true,
				'id' => 'DirectionForm'
			)
		);

		//	Скрытое поле для передачи данных
		//	Необходимо обдумать этот вариант подробнее
		echo '<input type="hidden" name="data[Direction][user_id]" class="form-control" id="DirectionUserId" value="'.$trainer_id.'">';
		
		echo $this->Widgets->Form(

			array(
				array(
					'type' => 'hidden',
					'name' => 'id',
					'value' => @$direction['id']
				),

				array(
					'type' => 'text',
					'name' => 'name',
					'label' => 'Название',
					'value' => @$direction['name'],
					'placeholder' => 'Введите название'
				)
			)
		);
		
		echo '<div class="status-form"></div>';

		echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list/'.$trainer_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
		echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button responsive-button green', 'div' => false, 'style' => 'margin-right: 25px;'));
		echo $this->Form->end();
		echo $this->Js->writeBuffer();
	?>

</div>