<div class="table-container">

	<h1>Загрузка видео-ролика</h1>
	<?php 
		echo $this->Form->create(
			false,
			array(
				'url' => array('controller' => 'directors', 'action' => 'video_add'),
				'default' => true
			)
		);

		echo $this->Form->input('Video', array(
			'type' => 'textbox',
			'class' => 'form-control',
			'label' => 'Вставьте код видео-ролика для загрузки',
			'placeholder' => '<iframe width="..." height="..." src="https://www.youtube.com/embed/..." frameborder="0" allowfullscreen></iframe>',
			'div' => 'input_container',
			'autocomplete' => 'off'
		));

		echo $this->Form->submit('Загрузить', array('class' => 'btn btn-primary button responsive-button green one', 'div' => "text-center"));
		echo $this->Form->end();
	?>

	<hr>

	<?php
		echo $this->element('video_list');
	?>

</div>