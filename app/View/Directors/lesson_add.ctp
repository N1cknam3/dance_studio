<div class="table-container text-center">

	<h2>Новое посещение</h2>
	
	<?php

		$ajax_data = $this->Js->get('#LessonForm')->serializeForm(
	                                                array(
	                                                'isForm' => true,
	                                                'inline' => true)
	                                            );
	 
	    // Submit the serialize data on submit click
	    $this->Js->get('#LessonForm')->event(
         	'submit',
         	$this->Js->request(
	            array(
		            'controller' => 'directors',
	            	'action' => 'ajax_lesson_data_check'
	            ),
	            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $ajax_data,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST',
                    'success' => 'form_success(data, ".status-form", "/directors/lesson_list/'.$trainer_id."/".$direction_id.'")'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
	        )
        );

		echo $this->Form->create(
			'Lesson',
			array(
				'url' => $this->Html->url(array('controller' => 'directors', 'action' => 'lesson_add')),
				'default' => true,
				'id' => 'LessonForm'
			)
		);

		//	Скрытое поле для передачи данных
		//	Необходимо обдумать этот вариант подробнее
		echo '<input type="hidden" name="data[Lesson][direction_id]" class="form-control" id="LessonDirectionId" value="'.$direction_id.'">';
		
		echo $this->Widgets->Form(

			array(
				array(
					'type' => 'hidden',
					'name' => 'id',
					'value' => @$form_data['id']
				),

				array(
					'type' => 'date',
					'name' => 'date',
					'label' => 'Дата',
					'value' => @$form_data['date']
				),

				array(
					'type' => 'checkbox',
					'name' => 'pupil',
					'label' => 'Ученики',
					'options' => @$form_data['pupils'],
					'selected' => @$form_data['selected']
				)
			)

		);
		
		echo '<div class="status-form"></div>';

		echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'lesson_list/'.$trainer_id."/".$direction_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
		echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button responsive-button green', 'div' => false, 'style' => 'margin-right: 25px;'));
		echo $this->Form->end();
		echo $this->Js->writeBuffer();
	?>

</div>