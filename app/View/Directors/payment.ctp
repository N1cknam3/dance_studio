<div class="table-container">

	<h2>Настройка платежей</h2>
	
	<div class="my-no-data">

		<?php

		$form_data = $last_payment;

		echo $this->Form->create(
			false,
			array(
				'url' => (array('controller' => 'directors', 'action' => 'save_payment')),
				'default' => true,
				'id' => 'StatisticsForm'
			)
		);

		echo $this->Widgets->Form(
			array(
				array(
					'type' => 'text',
					'name' => 'cost_lesson',
					'label' => 'Цена занятия',
					'value' => @$form_data['cost_lesson'],
					'default' => '200'
				),

				array(
					'type' => 'text',
					'name' => 'cost_trainer',
					'label' => 'ЗП преподавателя за ученика',
					'value' => @$form_data['cost_trainer'],
					'default' => '100'
				),

				array(
					'type' => 'text',
					'name' => 'cost_rent',
					'label' => 'Аренда зала',
					'value' => @$form_data['cost_rent'],
					'default' => '200'
				),

				array(
					'type' => 'text',
					'name' => 'cost_electricity',
					'label' => 'Плата за электричество',
					'value' => @$form_data['cost_electricity'],
					'default' => '2000'
				)
			)
		);

		echo $this->Form->submit('Сохранить', array('class' => 'btn btn-primary button responsive-button one green', 'div' => false));
		echo $this->Form->end();
	?>

	</div>
</div>