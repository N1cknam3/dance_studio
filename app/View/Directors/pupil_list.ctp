<div class="table-container">

	<h2>Список учеников направления <?php echo @$direction_name; ?></h2>
	<h4>Преподаватель: <?php echo $trainer_name; ?></h4>
	
	<?php
		if (empty($pupils)) {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Нет учеников");
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list/'.$trainer_id), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить ученика',													
					array('action' => 'pupil_add/'.$trainer_id."/".$direction_id),
					array(
						'escape' => false,
						'class' => 'btn btn-primary button responsive-button green'
				));
			echo '</div>';
		} else {
			echo $this->Widgets->table(
				array('Имя', 'Действия'),
				$pupils,
				array(
					'edit' => array(
						'action' => 'pupil_edit/'.$trainer_id."/".$direction_id,
						'title' => 'Редактировать данные ученика'
					),
					'delete' => array(
						'action' => 'pupil_delete/'.$trainer_id."/".$direction_id,
						'title' => 'Удалить ученика из группы'
					)
				)
			);

			echo '<div class="row group-button-container no-margin">';
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'direction_list/'.$trainer_id), array('class' => 'btn btn-primary button button_add lighter group-button col-md-3', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить ученика',													
					array('action' => 'pupil_add/'.$trainer_id."/".$direction_id),
					array(
						'escape' => false,
						'class' => 'button_add text-center col-md-9'
				));
			echo '</div>';
		}
	?>

</div>