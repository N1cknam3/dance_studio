<div class="table-container">
	<h2>Статистика</h2>
	<h4>Выберите преподавателя для просмотра подробной информации:</h4>
	<br>

	<div class="my-no-data">
		<?php

			if (!empty($trainers)) {

				echo $this->Form->create(
					false,
					array(
						'url' => array('controller' => 'directors', 'action' => 'statistics'),
						'default' => true,
						'id' => 'StatisticsForm'
					)
				);

				echo $this->Widgets->Form(
					array(
						array(
							'type' => 'select',
							'label' => 'Преподаватель',
							'name' => 'Trainer',
							'options' => @$trainers
						)
					)
				);

				echo $this->Form->submit('Далее', array('class' => 'btn btn-primary button responsive-button one green', 'div' => false));
				echo $this->Form->end();

			} else {
				echo $this->Widgets->no_data("Нет преподавателей");
				echo $this->Html->link('Управление преподавателями', array('action' => 'cabinet'), array('class' => 'btn btn-primary button responsive-button one green', 'div' => false, 'escape' => false));
			}
		?>
	</div>

</div>