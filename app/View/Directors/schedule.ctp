<div class="table-container">

	<h1>Загрузка расписания</h1>
	<?php 
		echo $this->Form->create('Schedule', array('url' => array('controller' => 'directors', 'action' => 'uploadSchedule'), 'type' => 'file', 'class'=>'dropzone', 'id'=>'my-dropzone'));
		echo $this->Form->end();
	?>

	<hr>

	<?php
		echo $this->element('schedule');
	?>

</div>