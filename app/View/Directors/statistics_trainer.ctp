<div class="table-container">
	<h2>Статистика преподавателя</h2>
	<h4>ПреподавателЬ: <?php echo $trainer['User']['name']; ?></h4>

	<hr>

	<?php
		if (!empty($directions)) {
	?>

		<h4>Выберите направление для просмотра подробной информации:</h4>
		<br>
			<div class="my-no-data">

			<?php

				echo $this->Form->create(
					false,
					array(
						'url' => array('controller' => 'directors', 'action' => 'statistics_trainer/' . $trainer['User']['id']),
						'default' => true,
						'id' => 'ChooseDirectionForm'
					)
				);

				echo $this->Widgets->Form(
					array(
						array(
							'type' => 'select',
							'label' => 'Направление',
							'name' => 'Direction',
							'options' => @$directions
						)
					)
				);
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'statistics'), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
				echo $this->Form->submit('Далее', array('class' => 'btn btn-primary button responsive-button green', 'div' => false));
				echo $this->Form->end();
			?>
				
			<hr>

			<h2>Статистика по направлениям</h2>

			<script>
				function get_ajax_trainer_statistics1() {
					$.ajax({
						type: "POST",
						
						url: "directors/get_ajax_trainer_statistics/" + <?php echo $trainer['User']['id']; ?>,
						
						data: {
							month: $(document).find("#Month").val(),
							year: $(document).find("#Year").val()
						},

						beforeSend: function() {
							var html_string = <?php echo "'" . $this->Widgets->GetLoadingAnimationHtml() . "'"; ?>;
							$("#ajax_directions_statistics_form").html(html_string);
						},
						
						success: function(data){
							$("#ajax_directions_statistics_form").html(data)
						},
				        error : function(data) {
				           alert("Ошибка получения данных по AJAX");
				        }
					});
				}
			</script>

			<?php

				$ajax_data = $this->Js->get('#DirectionStatistics')->serializeForm(
			                                                array(
			                                                'isForm' => true,
			                                                'inline' => true)
			                                            );
			 
			    // Submit the serialize data on submit click
			    $this->Js->get('#DirectionStatistics')->event(
		         	'submit',
		         	$this->Js->request(
			            array(
				            'controller' => 'directors',
			            	'action' => 'ajax_statistics_data_check'
			            ),
			            array(
		                    // 'update' => '#status_login', // element to update
		                                             			// after form submission
		                    'data' => $ajax_data,
		                    'async' => true,
		                    'dataExpression'=>true,
		                    'method' => 'POST'
		                    ,'success' => 'form_success(data, ".status-form", "")'
		                    // ,'error' => 'alert("Неправильный логин или пароль")'
		                )
			        )
		        );

				echo $this->Widgets->Form(
					array(
						array(
							'type' => 'date',
							'label' => 'Период отчета',
							'dateFormat' => 'MY',
							'value' => @$form_data['date']['value'],
							'minYear' => @$form_data['date']['minYear'],
							'maxYear' => @$form_data['date']['maxYear'],
							'others' => array(
								'onChange' => "get_ajax_trainer_statistics1()"
							)
						)
					)
				);
		
				echo '<div id="ajax_directions_statistics_form">';
					//	Динамически меняющаяся часть
					echo $this->element('trainer_directions_statistics');
				echo '</div>';
			?>
		</div>

		<?php
			} else {
		?>
				<div class="my-no-data">
					<?php
						echo $this->Widgets->no_data("Нет направлений");
						echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'statistics'), array('class' => 'btn btn-primary button responsive-button one', 'div' => false, 'escape' => false));
					
					?>
				</div>
		<?php
			}
		?>
</div>