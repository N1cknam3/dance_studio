<div class="table-container">

	<h2>Список танцевальных направлений</h2>
	<h4>Преподаватель: <?php echo $trainer_name; ?></h4>
	
	<?php
		if (empty($directions)) {
			echo '<div class="my-no-data">';
				echo $this->Widgets->no_data("Еще нет танцевальных направлений!");
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'trainers_list'), array('class' => 'btn btn-primary button responsive-button', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить направление',													
					array('action' => 'direction_add/'.$trainer_id),
					array(
						'escape' => false,
						'class' => 'btn btn-primary button responsive-button green'
				));
			echo '</div>';
		} else {
			echo $this->Widgets->table(
				array('Направление', 'Действия'),
				$directions,
				array(
					'table' => array(
						'action' => 'lesson_list/'.$trainer_id,
						'title' => 'Открыть список посещений'
					),
					'list' => array(
						'action' => 'pupil_list/'.$trainer_id,
						'title' => 'Открыть список учеников'
					),
				)
			);

			echo '<div class="row group-button-container no-margin">';
				echo $this->Html->link('<i class="fa fa-arrow-left"></i>Назад', array('action' => 'trainers_list'), array('class' => 'btn btn-primary button button_add lighter group-button col-md-3', 'div' => false, 'escape' => false));
				echo $this->Html->link(
					'<i class="fa fa-plus"></i> Добавить направление',													
					array('action' => 'direction_add/'.$trainer_id),
					array(
						'escape' => false,
						'class' => 'button_add text-center col-md-9'
				));
			echo '</div>';
		}
	?>

</div>