<div class="col-md-6">
	<div class="pbc-margin-left-zero post-bottom-content">
		<?php
			echo $this->Html->image("contents/news/1.jpg", array(
			    'style' => 'width: 100%;'
			));
		?>
		<h3>Самый большой выбор танцевальных направлений<!--  <span class="travel-bg">4<a class="travel-bg" href="#0">4 comment</a></span> --></h3>
		<span class="date">Вот сто процентов тут не хватает текста</span>

		<!-- ___Post Meta___ -->
		<!-- <div class="post-meta">
			<a href="#0" class="share-icon">
				<i class="fa fa-share-alt"></i>
				<span>25 Share</span>
			</a>
			<a href="#0" class="category-icon pull-right">
				<span>Travel</span>
				<i class="fa fa-plane"></i>
			</a>
		</div> -->
	</div><!-- End Post Bottom Content -->
</div><!-- End Column -->

<div class="col-md-6 space-md-fix">
	<div class="pbc-margin-right-zero post-bottom-content">
		<?php
			echo $this->Html->image("contents/news/2.jpg", array(
			    'style' => 'width: 100%;'
			));
		?>
		<h3>Выступления<!--  <span class="code-bg">4<a class="code-bg" href="#0">4 comment</a></span> --></h3>
		<span class="date">Здесь мог бы быть ваш текст</span>

		<!-- ___Post Meta___ -->
		<!-- <div class="post-meta">
			<a href="#0" class="share-icon">
				<i class="fa fa-share-alt"></i>
				<span>25 Share</span>
			</a>

			<a href="#0" class="category-icon pull-right">
				<span>Code</span>
				<i class="fa fa-code"></i>
			</a>
		</div> -->
	</div><!-- End Post Bottom Content -->
</div><!-- End Column -->

<div class="col-md-8 text-center space-md-fix">
	<div class="prc-margin-left-zero post-right-content">
		<div class="media">
			<div class="media-left no-padding">
				<?php
					echo $this->Html->image("contents/news/3.jpg", array(
						'class' => 'img-responsive',
					    'style' => 'width: 100%;'
					));
				?>
			</div>
			<div class="media-body">
				<h3>ОЦ "Монолит"<!--  <span class="music-bg">4<a class="music-bg" href="#0">4 comment</a></span> --></h3>
				<span class="date">А если бы тут был текст, то выглядело бы лаконично</span>

				<!-- ___Post Meta___ -->
				<!-- <div class="post-meta">
					<a href="#0" class="share-icon">
						<i class="fa fa-share-alt"></i>
						<span>25 Share</span>
					</a>

					<a href="#0" class="category-icon pull-right">
						<span>Music</span>
						<i class="fa fa-music"></i>
					</a>
				</div> -->
			</div>
		</div>
	</div><!-- End Post Right Content -->
</div><!-- End Column -->

<div class="col-md-4 text-center space-md-fix">
	<div class="poc-right-fix post-only-content">
		<h3>Текст <!-- <span class="mobile-bg">4<a class="mobile-bg" href="#0">4 comment</a></span> --></h3>
		<span class="date">Тут можно привести небольшой текст</span>

		<!-- ___Post Meta___ -->
		<!-- <div class="post-meta">
			<a href="#0" class="share-icon">
				<i class="fa fa-share-alt"></i>
				<span>25 Share</span>
			</a>

			<a href="#0" class="category-icon pull-right">
				<span>Mobile</span>
				<i class="fa fa-mobile"></i>
			</a>
		</div> -->
	</div><!-- End Post Only Content -->
</div><!-- End Column -->

<div class="col-md-6">
	<div class="pbc-margin-left-zero post-bottom-content">
		<?php
			echo $this->Html->image("contents/news/4.jpg", array(
			    'style' => 'width: 100%;'
			));
		?>
		<h3>Мастер-классы<!--  <span class="travel-bg">4<a class="travel-bg" href="#0">4 comment</a></span> --></h3>
		<span class="date">Интересно, что можно написать про мастер-классы?</span>

		<!-- ___Post Meta___ -->
		<!-- <div class="post-meta">
			<a href="#0" class="share-icon">
				<i class="fa fa-share-alt"></i>
				<span>25 Share</span>
			</a>
			<a href="#0" class="category-icon pull-right">
				<span>Travel</span>
				<i class="fa fa-plane"></i>
			</a>
		</div> -->
	</div><!-- End Post Bottom Content -->
</div><!-- End Column -->

<div class="col-md-6 space-md-fix">
	<div class="pbc-margin-right-zero post-bottom-content">
		<?php
			echo $this->Html->image("contents/news/5.jpg", array(
			    'style' => 'width: 100%;'
			));
		?>
		<h3>Ежегодные отчетные концерты<!--  <span class="code-bg">4<a class="code-bg" href="#0">4 comment</a></span> --></h3>
		<span class="date">Пожалуй, лучшие концерты в мире!</span>

		<!-- ___Post Meta___ -->
		<!-- <div class="post-meta">
			<a href="#0" class="share-icon">
				<i class="fa fa-share-alt"></i>
				<span>25 Share</span>
			</a>

			<a href="#0" class="category-icon pull-right">
				<span>Code</span>
				<i class="fa fa-code"></i>
			</a>
		</div> -->
	</div><!-- End Post Bottom Content -->
</div><!-- End Column -->