<?php
	$phoenix_vk_href = "http://vk.com/kurskdance";
	$phoenix_email = "phoenix_dance@inbox.ru";
	$phoenix_adress = "г. Курск, ул. Дзержинского 60, 3 этаж";
	$phoenix_telephone = "311-003";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!--Адаптация разрешения под разрешение мобильного устройства-->	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $this->fetch('title'); ?></title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('animate');
		echo $this->Html->css('bootstrap.min');
		// echo $this->Html->css('cake.generic');
		echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('highlight');
		echo $this->Html->css('meanmenu');
		echo $this->Html->css('owl.carousel');
		echo $this->Html->css('owl.theme');
		echo $this->Html->css('Pe-icon-7-stroke');
		echo $this->Html->css('responsive');
		echo $this->Html->css('style');
		
		echo $this->Html->css('nickname/style');
		echo $this->Html->css('nickname/table-container');

		echo $this->Html->script('vendor/jquery-1.11.3');
		echo $this->Html->script('vendor/modernizr-2.6.2.min');
		echo $this->Html->script('vendor/waypoints.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('circles.min');
		echo $this->Html->script('highlight');
		echo $this->Html->script('jquery.fitvids');
		echo $this->Html->script('jquery.jscroll');
		echo $this->Html->script('jquery.meanmenu.min');
		// echo $this->Html->script('map-script');
		echo $this->Html->script('owl.carousel.min');
		echo $this->Html->script('scripts');
		echo $this->Html->script('scrollIt.min');
		echo $this->Html->script('wow.min');

		echo $this->Html->script('nickname/functions');
		echo $this->Html->script('nickname/helper-functions');
		echo $this->Html->script('nickname/table-container');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<?php /* Строка адреса для использования в AJAX-запросах */?>
	<base href="<?php echo Router::url('/'); ?>" />

</head>
<body>

<?php
    $data3 = $this->Js->get('#LoginForm')->serializeForm(
                                                array(
                                                'isForm' => true,
                                                'inline' => true)
                                            );
 
    // Submit the serialize data on submit click
    $this->Js->get('#LoginForm')->event(
          'submit',
          $this->Js->request(
            array(
	            'controller' => 'users',
            	'action' => 'ajax_login_check'
            ),
            array(
                    // 'update' => '#status_login', // element to update
                                             			// after form submission
                    'data' => $data3,
                    'async' => true,
                    'dataExpression'=>true,
                    'method' => 'POST'
                    ,'success' => 'login_success(data)'
                    // ,'error' => 'alert("Неправильный логин или пароль")'
                )
            )
        );
?>

	<!-- ___Start Home One Page___ -->
	<div class="container-fluid home-1" id="container-full">
		<div class="row">

			<!-- ___Start Left Menu___ -->
			<div class="col-md-2 no-padding">
				<div id="left-sidebar" style="position: fixed; top: 0px; left: 0px; width: 16.66666667%; ">
					<div class="sidebar-menu">
						<div class="logo">
							<?php
								echo $this->Html->image("logo-phoenix-text.png", array(
								    "alt" => "Танцевальная студия",
								    'url' => array('controller' => 'users', 'action' => 'main')
								));
							?>
						</div>

						<!-- ___Start Menu Area___ -->
						<div id="menu-area" class="menu-area toogle-sidebar-sections">
							<div class="menu-head">
								<a href="#0" class="accordion-toggle">Меню <span class="toggle-icon"><i class="fa fa-bars"></i></span></a>
								<div class="accordion-content">
									<div class="menu-body">
										<ul>
											<li class="home">
												<?php
													echo $this->Html->link(
														'Главная',													
														array('action' => 'main')
													);
												?>
												<!-- <ul class="drop-menu">
													<li><a href="index.html">Home Layout - 1</a></li>
													<li><a href="home-2.html">Home Layout - 2</a></li>
													<li><a href="home-3.html">Home Layout - 3</a></li>
													<li><a href="home-4.html">Home Layout - 4</a></li>
													<li><a href="home-5.html">Home Layout - 5</a></li>
													<li><a href="home-6.html">Home Layout - 6</a></li>
												</ul> -->
											</li>
											<li>
												<?php
													echo $this->Html->link(
														'Видео',													
														array('action' => 'video')
													);
												?>
											</li>
											<li>
												<?php
													echo $this->Html->link(
														'Расписание',													
														array('action' => 'schedule')
													);
												?>
											</li>
											<!-- <li><a href="about.html">О нас</a></li>
											<li><a href="contact.html">Контакты</a></li> -->
										</ul>
									</div><!-- End Menu Body -->
								</div><!-- End According Content -->
							</div><!-- End Menu Head -->
						</div>
						<!-- End Menu Area -->

					</div><!-- End Sidebar Menu -->
				</div><!-- End Menu Left -->
			</div><!-- End Column -->
			<!-- End Left Menu -->

			<!-- ___Start Column___ -->
			<div class="col-md-10 no-padding">

				<!-- ___Start Top Bar___ -->
				<div class="top-bar">
					<div class="top-bar-head">
						<div class="search">
							<i class="pe-7s-user showSingle" id="2"></i>
							<p>Добро пожаловать в тусовку!</p>
						</div>
						<div class="login-user pull-right showSingle" id="3">
							<i class="pe-7s-mail"></i>
						</div>

						<!-- <div class="login-mail pull-right showSingle" id="2">
							<i class="pe-7s-user active"></i>
						</div> -->
					</div>
					<!-- End Top Bar Head -->

					<!-- ___Start Top Bar Body___ -->
					<div class="top-bar-body">
						<div class="search-body targetDiv" id="div1">
							<p>Что ищешь?</p>
							<form>
								<input type="text" class="form-control no-radius" placeholder="Вводи здесь |">
							</form>
						</div>

						<!-- ___Start Top Bar Login Body___ -->
						<div class="user-body targetDiv" id="div2">
							<div class="row">
								<div class="col-md-6">
									<div class="login-account">
										<p>Вход на сайт</p>
										<?php echo $this->Form->create('User', array('url'=>array('controller'=>'students', 'action'=>'cabinet'), 'id' => 'LoginForm', 'default' => false)); ?>
											<div class="form-group">
												<?php
													echo $this->Form->input('email', array(
														'label'=>false,
														'div'=>false,
														'class'=>'form-control no-radius',
														'placeholder'=>'Логин'
													));
												?>
											</div>
											<div class="input-group">
												<?php
													echo $this->Form->input('password', array(
														'label'=>false,
														'div'=>false,
														'class'=>'form-control no-radius',
														'placeholder'=>'Пароль'
													));
												?>
												<span class="input-group-btn">
													<?php echo $this->Form->button('OK', array('type'=>'submit', 'class'=>'btn btn-default', 'div'=>false)); ?>
												</span>
											</div><!-- Input Group -->
											<!-- <div class="forgot-pass">Забыл <a href="#0">пароль?</a></div> -->
										<?php
											echo $this->Form->end();
										?>
										<div id="status_login" class="status">123</div>
									</div><!-- End Login Account -->
								</div><!-- End Column -->

								<!-- <div class="dashed-divider"></div>
								<div class="col-md-6">
									<div class="register">
										<p>Первый раз тут?</p>
										<button type="submit" class="btn btn-default form-control no-radius">Регистрация</button>
									</div>
								</div> --><!-- End Column -->
							</div><!-- End Row -->
						</div>
						<!-- End Top Bar Login Body -->

						<!-- ___Start Mail Body___ -->
						<div class="mail-body targetDiv" id="div3">
							<div class="row">

								<!-- ___Start Mail Contact___ -->
								<div class="col-md-6">
									<div class="mail-contact">
										<h4>Связаться с нами</h4>
										<div class="row">
											<div class="col-md-3 col-xs-4 no-padding">
												<p class="address"><strong>Адрес :</strong></p>
												<p><strong>Email :</strong> </p>
												<p><strong>Тел. :</strong> </p>
											</div>
											<div class="col-md-9 col-xs-8 no-padding">
												<p class="address"><?php echo "$phoenix_adress"; ?></p>
												<p><?php echo "$phoenix_email"; ?></p>
												<p><?php echo "$phoenix_telephone"; ?></p>
											</div>
										</div>
									</div><!-- End Mail Contact -->
								</div><!-- End Column -->

								<div class="dashed-divider"></div>
								<!-- ___Start Follow Us___ -->
								<div class="col-md-6">
									<div class="mail-follow-us">
										<h4>Присоединиться к тусовке</h4>
										<ul>
											<li>
												<a href=<?php echo "\"$phoenix_vk_href\""; ?> class="connect-with-us facebook">
													<i class="fa fa-vk"></i>
													<span>Вперед</span>
													<div class="plus">
														<i class="fa fa-plus"></i>
													</div>
												</a><!-- End Facebook -->
											</li>
										</ul>
									</div><!-- End Mail Follow us -->
								</div><!-- End Column -->
							</div><!-- End Row -->
						</div><!-- End Mail Body -->
					</div><!-- End Top Bar Body -->
				</div>
				<!-- End Top Bar -->

				<!-- ___Main Content___ -->
				<div class="main-content" style="padding: 50px 85px; ">
					
					<!-- ___Mani Post Body___ -->
					<div class="main-post-body" style="margin-top: 0px; ">
						<div class="row">

							<div class="col-md-12 no-padding w-100">
								<div class="row no-margin">
									
									<?php echo $this->fetch('content'); ?>

								</div> <!-- End Row -->
							</div>

						</div>
					</div>

				</div><!-- End Main Content -->

				<!-- ___Start Bottom___ -->
				<div class="bottom container-fluid">
					<div class="row">
						<!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="useful-links widget">
								<h3>Полезные ссылки</h3>
								<ul class="pull-left">
									<li><a href="index.html">Главная</a></li>
									<li><a href="about.html">О нас</a></li>
									<li><a href="contact.html">Контакты</a></li>
								</ul>
								<ul class="pull-right">
									<li><a href="elements.html">Школа</a></li>
									<li><a href="#0">Условия</a></li>
									<li><a href="#0">Наши достижения</a></li>
								</ul>
							</div>
						</div> -->
						<!-- End Column -->

						<!-- ___Contact Us___ -->
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="bottom-contact widget">
								<h3>Контакты</h3>
								<div class="contact-info">
									<p><strong>Адрес :</strong><?php echo "$phoenix_adress"; ?></p>
									<p><strong>Email :</strong><?php echo "$phoenix_email"; ?></p>
									<p><strong>Тел. :</strong><?php echo "$phoenix_telephone"; ?></p>
								</div>
							</div>
						</div>
						<!-- End Column -->

						<!-- ___News Letter___ -->
						<!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
							<div class="newsletter widget">
								<h3>Подписка</h3>
								<div class="input-group">
									<input type="text" class="form-control no-radius" placeholder="Email">
									<span class="input-group-btn  no-radius">
										<button class="btn btn-default" type="button">Подписаться</button>
									</span>
								</div>
								<p>Подпишись, чтобы получать свежие новости через email.</p>
							</div>
						</div> -->
						<!-- End Column -->

						<!-- ___Start Social Icons Column___ -->
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<!-- ___Start Social Icons___ -->
							<div class="bottom-social widget">
								<h3>Мы в интернете</h3>
								<div class="social-icon">
									<ul>
										<li>
											<a href=<?php echo "\"$phoenix_vk_href\""; ?> class="connect-with-us facebook">
												<i class="fa fa-vk"></i>
											</a>
										</li>
									</ul>
								</div>
								<p>Присоединяйся к нам, чтобы быть в курсе всего нового</p>
							</div><!-- End Social Icons -->
						</div><!-- End Column -->
					</div><!-- End Row -->
				</div>
				<!-- End Bottom -->

				<div class="footer text-center">
					<p>© 2015 Ефимова Татьяна. Все права защищены.</p>
				</div>

				
			</div><!-- End Column -->
			
		</div><!-- End Row -->
	</div><!-- End Container -->

	<?php
		echo $this->Js->writeBuffer();
	?>
	
</body>
</html>
