<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('AdminsController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TrainersController extends AdminsController {

	public function beforeRender() {
		if ($this->isLogin() && $this->isLoginType(1)) {
			if( !$this->request->is('ajax') ) {
				$this->layout = 'trainers-cabinet';
			}
			$this->set("title_for_layout", "Танцевальная студия");

			$user = $this->getUser();
			$this->set('name', $user['User']['name']);
			$this->set('email', $user['User']['email']);
			$this->set('message', $this->getMessage());
		} else {
			$this->redirectToRoot();
		}
	}

	public function cabinet() {
		$this->redirect('direction_list');
	}

	private function getMessage() {
		$messages = array('Радовать учеников', 'Прорабатывать низ', 'Заняться растяжкой');
		$num = rand(0, count($messages) - 1);
		return $messages[$num];
	}

	public function direction_list() {
		$user = $this->getUser();
		$directions = $this->getElementsFromModel('all', 'Direction', array('user_id' => $user['User']['id']), array('id', 'name'));
		$directions = $this->redunantModelName($directions, 'Direction');
		$this->set('directions', $directions);
	}

	public function pupil_list($direction_id) {
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$pupils = $this->getElementsFromModel('all', 'Pupil', array('direction_id' => $direction_id, 'status' => 1), array('id', 'name'));
		$pupils = $this->redunantModelName($pupils, 'Pupil');
		$this->set('pupils', $pupils);
	}

	public function ajax_pupil_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['Pupil']['id'];
		$name = str_replace(' ', '', $this->request->data['Pupil']['name']);
		// $direction_id = $this->request->data['Pupil']['direction_id'];

		if (!$name) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {
			if ($id) {
				$this->updateElementToModel($id, $this->request->data['Pupil'], 'Pupil');
			} else {
				$this->createElementToModel($this->request->data['Pupil'], 'Pupil');
			}
			exit;
		}
	}

	public function pupil_add($direction_id) {
		$this->set('direction_id', $direction_id);

		$user = $this->getUser();
		$directions = $this->getElementsFromModel('list', 'Direction', array('user_id' => $user['User']['id']));
		$pupil_data = array('direction_id' => $direction_id, 'directions' => $directions);
		$this->set('pupil', $pupil_data);
	}

	public function pupil_edit($direction_id, $pupil_id) {
		$this->set('direction_id', $direction_id);

		$user = $this->getUser();
		$directions = $this->getElementsFromModel('list', 'Direction', array('user_id' => $user['User']['id']));
		$pupil = $this->getElementsFromModel('first', 'Pupil', array('id' => $pupil_id));
		$pupil_data = array('direction_id' => $direction_id, 'directions' => $directions, 'name' => $pupil['Pupil']['name'], 'id' => $pupil['Pupil']['id']);
		$this->set('pupil', $pupil_data);
	}

	public function pupil_delete($direction_id, $pupil_id) {

		//	Смотрим, есть ли посещения этого ученика
		$pupilsLessons = $this->getElementsFromModel('all', 'PupilLesson', array('pupil_id' => $pupil_id));

		if (empty($pupilsLessons)) {
			//	Удаляем ученика, если у него не было посещений
			$this->deleteElementFromModel($pupil_id, 'Pupil');
		} else {
			//	Иначе не удаляем ученика, а делаем его неактивным
			$this->updateElementToModel($pupil_id, array('status' => 0), 'Pupil');
		}

		$this->redirect('pupil_list/'.$direction_id);
	}

	public function lesson_list($direction_id) {
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$table_data = $this->getElementsFromModel('all', 'Lesson', array('direction_id' => $direction_id), array('id', 'date'));
		$table_data = $this->redunantModelName($table_data, 'Lesson');
		$this->set('table_data', $table_data);
	}

	public function ajax_lesson_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['Lesson']['id'];
		$date = $this->request->data['Lesson']['date'];

		if (!$date) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {
			if ($id) {
				$pupils = $this->request->data['Lesson']['pupil'];
				unset($this->request->data['Lesson']['pupil']);

				$this->updateElementToModel($id, $this->request->data['Lesson'], 'Lesson');
				//	Обновление информации о выбранных студентах
				$this->updatePupilLessons($id, $pupils);
			} else {
				$pupils = $this->request->data['Lesson']['pupil'];
				unset($this->request->data['Lesson']['pupil']);

				$new_lesson_id = $this->createElementToModel($this->request->data['Lesson'], 'Lesson');
				//	Обновление информации о выбранных студентах
				$this->updatePupilLessons($new_lesson_id, $pupils);
			}
			exit;
		}
	}

	public function updatePupilLessons($lesson_id, $newPupilsIdArray) {
		//	Удаление всех учеников до изменения
		$pupilsLessons = $this->getElementsFromModel('all', 'PupilLesson', array('lesson_id' => $lesson_id));
		$pupilsLessons = $this->redunantModelName($pupilsLessons, 'PupilLesson');
		foreach ($pupilsLessons as $pupil_lesson) {
			$this->deleteElementFromModel($pupil_lesson['id'], 'PupilLesson');
		}

		//	Сохранение выбранных сейчас учеников
		if ($newPupilsIdArray && !empty($newPupilsIdArray))
			foreach ($newPupilsIdArray as $pupil_id) {
				$this->createElementToModel(array('pupil_id' => $pupil_id, 'lesson_id' => $lesson_id), 'PupilLesson');
			}
	}

	public function lesson_add($direction_id) {
		$this->set('direction_id', $direction_id);

		$pupils = $this->getElementsFromModel('list', 'Pupil', array('direction_id' => $direction_id, 'status' => 1));
		$form_data = array('pupils' => $pupils);
		$this->set('form_data', $form_data);
	}

	public function lesson_edit($direction_id, $lesson_id) {
		$this->set('direction_id', $direction_id);

		$pupils = $this->getElementsFromModel('list', 'Pupil', array('direction_id' => $direction_id, 'status' => 1));
		$lesson = $this->getElementsFromModel('first', 'Lesson', array('id' => $lesson_id));

		//	Создание массива отмеченных учеников
		$sel_array = array();
		$selected = $lesson['Pupil'];
		foreach ($selected as $value) {
			array_push($sel_array, $value['id']);
		}
		//	--- готово

		$lesson_data = array('date' => $lesson['Lesson']['date'], 'id' => $lesson['Lesson']['id'], 'pupils' => $pupils, 'selected' => $sel_array);
		$this->set('form_data', $lesson_data);
	}

	public function lesson_delete($direction_id, $lesson_id) {
		$this->deleteElementFromModel($lesson_id, 'Lesson');
		//	Удаление ссылок на учеников, посетивших занятие
		$this->updatePupilLessons($lesson_id, null);
		$this->redirect('lesson_list/'.$direction_id);
	}

	public function direction_chart($direction_id) {
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$chart_data = $this->get_direction_chart_data($direction_id);
		$this->set('chart_data', $chart_data);
	}

}