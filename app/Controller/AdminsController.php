<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('UsersController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AdminsController extends UsersController {

	public function beforeRender() {
		if ($this->isLogin()) {
			$this->set("title_for_layout", "Танцевальная студия");
		} else {
			$this->redirectToRoot();
		}
	}

	//	массив количества занятий в формате - ['date', 'count']
	protected function get_direction_chart_data($direction_id, $date_from = null, $date_to = null) {

		if (($date_from != null) and ($date_to != null)) {
			$condition = array(
				'direction_id' => $direction_id,
				'and' => array(
					'date >=' => $date_from,
					'date <' => $date_to,
				)
			);
		} else {
			$condition = array(
				'direction_id' => $direction_id
			);
		}

		$lessons = $this->getElementsFromModel(
			'all',
			'Lesson',
			$condition,
			array('id', 'date')
		);
		$lessons = $this->redunantModelName($lessons, 'Lesson');
		$this->set('lessons', $lessons);

		$chartDataArray = array();
		foreach ($lessons as $key => $value) {
			$pupils = $this->getElementsFromModel('all', 'PupilLesson', array('lesson_id' => $value['id']));
			array_push($chartDataArray, array("date" => $value['date'], "count" => count($pupils)));
		}

		return $chartDataArray;
	}

	//	массив количества занятий в формате - ['name', 'data' => array(['title', 'value'], ...)]
	protected function get_directions_chart_data_visits($trainer_id, $date_from = null, $date_to = null) {

		$directions = $this->getElementsFromModel('all', 'Direction', array('user_id' => $trainer_id));
		$directions = $this->redunantModelName($directions, 'Direction');

		//	Настройки поиска общие
		if (($date_from != null) and ($date_to != null)) {
			$condition = array(
				'and' => array(
					'date >=' => $date_from,
					'date <' => $date_to,
				)
			);
		} else {
			$condition = array();
		}
		
		$answer = array('name' => 'Количество посещений', 'data' => array(), 'type' => 'pie');

		if (!empty($directions)) {
			foreach($directions as $direction) {

				//	Настройки поиска под определенное направление
				$condition['direction_id'] = $direction['id'];

				//	Получение всех занятий этого направления
				$lessons = $this->getElementsFromModel(
					'all',
					'Lesson',
					$condition
				);

				//	Количество посещений этого направления
				$visits_count = 0;
				foreach ($lessons as $value) {
					$visits_count += count($value['Pupil']);
				}

				array_push($answer['data'], array('title' => $direction['name'], 'value' => $visits_count));
			}
		}

		return $answer;
	}

	public function video() {
		parent::video();
		$this->set('video_delete', true);
	}

	public function video_add() {
		$video_html = $this->request->data['Video'];
		$this->createElementToModel(array('html' => $video_html), 'Video');
		$this->redirectToAction('video');
	}

	public function video_delete($video_id) {
		$this->deleteElementFromModel($video_id, 'Video');
		$this->redirectToAction('video');
	}

	public function redirectToAction($action) {
		if ($this->isLoginType(0))
			$this->redirect(array('controller' => 'directors', 'action' => $action));
		else if ($this->isLoginType(1))
			$this->redirect(array('controller' => 'trainers', 'action' => $action));
		else
			$this->redirectToRoot();
	}

}