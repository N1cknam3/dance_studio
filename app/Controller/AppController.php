<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	protected function redirectToRoot() {
		$this->redirect('/');
	}

	protected function ajaxMethodException() {
		throw new UnauthorizedException(__('Запрашиваемая страница недоступна'));
	}

	public function check_email($email) { 
		if(!eregi("^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]{2,4}$", $email)) {
			 	return false;	
		} 		
		else {
			return true;
		}
	}

	//	Удаление элемента из модели
	protected function deleteElementFromModel($elem_id, $modelName) {
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден '.$modelName.' с ID='.$elem_id));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->$modelName->delete()) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно удалить запись с ID='.$elem_id.' из "'.$modelName.'"!'); // 500 error
		}
	}
	
	//	Создание нового элемента в модели $modelName с данными $data
	protected function createElementToModel($data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->create();
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return $this->$modelName->id;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно сохранить данные в "'.$modelName.'"!'); // 500 error
		}
	}
	
	//	Обновление данных элемента в модели $modelName данными $data
	protected function updateElementToModel($elem_id, $data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден '.$modelName.' с ID='.$elem_id));
		}
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException('Невозможно обновить запись с ID='.$elem_id.' в "'.$modelName.'"!'); // 500 error
		}
	}
	
	//	Получение данных из модели
	protected function getElementsFromModel($findType, $modelName, $conditions = null, $fields = null, $others = null) {
		$this->loadModel($modelName);
		$options = array();
		if ($conditions != null)
			$options['conditions'] = $conditions;
		if ($fields != null)
			$options['fields'] = $fields;
		if ($others != null)
			$options += $others;
		return $this->$modelName->find($findType, $options);
	}

	//	Очистка уровня массива - имени модели
	protected function redunantModelName($data, $modelName) {
		return Set::extract('/'.$modelName.'/.', $data);
	}
	
	//	Вывод список SQL запросов модели
	protected function getSQLlog($model)
	{
		$this->loadModel($model);
		$log = $this->$model->getDataSource()->getLog(false, false);
		debug($log);
	}

}
