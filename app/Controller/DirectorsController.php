<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('AdminsController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DirectorsController extends AdminsController {

	public function beforeRender() {

		$locale = Configure::read('Config.language');	
		$this->set('locale', $locale);

		if ($this->isLogin() && $this->isLoginType(0)) {
			if( !$this->request->is('ajax') ) {
				$this->layout = 'directors-cabinet';
			}

			$this->set("title_for_layout", "Танцевальная студия");

			$user = $this->getUser();
			$this->set('name', $user['User']['name']);
			$this->set('email', $user['User']['email']);
		} else {
			$this->redirectToRoot();
		}
	}

	public function cabinet() {
		$this->redirect('trainers_list');
	}

	public function trainers_list() {
		$data = $this->getElementsFromModel('all', 'Users', array('type' => 1), array('id', 'name'));
		$data = $this->redunantModelName($data, 'Users');
		$this->set('data', $data);
	}

	public function trainer_add() {
		
	}

	public function ajax_trainer_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['User']['id'];
		$name = str_replace(' ', '', $this->request->data['User']['name']);
		$email = str_replace(' ', '', $this->request->data['User']['email']);
		$password = str_replace(' ', '', $this->request->data['User']['password']);

		if (!$name || !$email || !$password) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {

			if (!$this->check_email($email)) {
				$this->set('error_message', self::ERR_EMAIL);
			} else {

				//	Предотвращаем создание преподавателей с одинаковыми e-mail
				if ($id)
					$user = $this->getElementsFromModel('first', 'User', array('email' => $email, 'id !=' => $id));
				else
					$user = $this->getElementsFromModel('first', 'User', array('email' => $email));

				if (!empty($user)) {
					$this->set('error_message', self::ERR_USER_EXISTS);
				} else {

					$data = $this->request->data['User'];
					$data['type'] = 1;

					if ($id) {
						$this->updateElementToModel($id, $data, 'User');
					} else {
						$this->createElementToModel($data, 'User');
					}
					exit;
				}
			}
		}
	}

	public function trainer_edit($trainer_id) {
		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer', $trainer["User"]);
	}

	public function trainer_delete($trainer_id) {
		$this->deleteElementFromModel($trainer_id, 'User');
		$this->redirect('trainers_list');
	}

	public function direction_list($trainer_id) {
		$this->set('trainer_id', $trainer_id);
		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer_name', $trainer['User']['name']);

		$directions = $this->getElementsFromModel('all', 'Direction', array('user_id' => $trainer_id), array('id', 'name'));
		$directions = $this->redunantModelName($directions, 'Direction');
		$this->set('directions', $directions);
	}

	public function direction_add($trainer_id) {
		$this->set('trainer_id', $trainer_id);
	}

	public function ajax_direction_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['Direction']['id'];
		$name = str_replace(' ', '', $this->request->data['Direction']['name']);

		if (!$name) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {

			$data = $this->request->data['Direction'];

			if ($id) {
				$this->updateElementToModel($id, $data, 'Direction');
			} else {
				$this->createElementToModel($data, 'Direction');
			}
			exit;
		}
	}

	public function pupil_list($trainer_id, $direction_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer_name', $trainer['User']['name']);

		$pupils = $this->getElementsFromModel('all', 'Pupil', array('direction_id' => $direction_id, 'status' => 1), array('id', 'name'));
		$pupils = $this->redunantModelName($pupils, 'Pupil');
		$this->set('pupils', $pupils);
	}

	public function ajax_pupil_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['Pupil']['id'];
		$name = str_replace(' ', '', $this->request->data['Pupil']['name']);
		// $direction_id = $this->request->data['Pupil']['direction_id'];

		if (!$name) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {
			if ($id) {
				$this->updateElementToModel($id, $this->request->data['Pupil'], 'Pupil');
			} else {
				$this->createElementToModel($this->request->data['Pupil'], 'Pupil');
			}
			exit;
		}
	}

	public function pupil_add($trainer_id, $direction_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$directions = $this->getElementsFromModel('list', 'Direction', array('user_id' => $trainer_id));
		$pupil_data = array('direction_id' => $direction_id, 'directions' => $directions);
		$this->set('pupil', $pupil_data);
	}

	public function pupil_edit($trainer_id, $direction_id, $pupil_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$directions = $this->getElementsFromModel('list', 'Direction', array('user_id' => $trainer_id));
		$pupil = $this->getElementsFromModel('first', 'Pupil', array('id' => $pupil_id));
		$pupil_data = array('direction_id' => $direction_id, 'directions' => $directions, 'name' => $pupil['Pupil']['name'], 'id' => $pupil['Pupil']['id']);
		$this->set('pupil', $pupil_data);
	}

	public function pupil_delete($trainer_id, $direction_id, $pupil_id) {

		//	Смотрим, есть ли посещения этого ученика
		$pupilsLessons = $this->getElementsFromModel('all', 'PupilLesson', array('pupil_id' => $pupil_id));

		if (empty($pupilsLessons)) {
			//	Удаляем ученика, если у него не было посещений
			$this->deleteElementFromModel($pupil_id, 'Pupil');
		} else {
			//	Иначе не удаляем ученика, а делаем его неактивным
			$this->updateElementToModel($pupil_id, array('status' => 0), 'Pupil');
		}

		$this->redirect('pupil_list/'.$trainer_id."/".$direction_id);
	}

	public function lesson_list($trainer_id, $direction_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer_name', $trainer['User']['name']);

		$table_data = $this->getElementsFromModel('all', 'Lesson', array('direction_id' => $direction_id), array('id', 'date'));
		$table_data = $this->redunantModelName($table_data, 'Lesson');
		$this->set('table_data', $table_data);
	}

	public function ajax_lesson_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$id = $this->request->data['Lesson']['id'];
		$date = $this->request->data['Lesson']['date'];

		if (!$date) {
			$this->set('error_message', self::ERR_FIELDS);
		} else {
			if ($id) {
				$pupils = $this->request->data['Lesson']['pupil'];
				unset($this->request->data['Lesson']['pupil']);

				$this->updateElementToModel($id, $this->request->data['Lesson'], 'Lesson');
				//	Обновление информации о выбранных студентах
				$this->updatePupilLessons($id, $pupils);
			} else {
				$pupils = $this->request->data['Lesson']['pupil'];
				unset($this->request->data['Lesson']['pupil']);

				$new_lesson_id = $this->createElementToModel($this->request->data['Lesson'], 'Lesson');
				//	Обновление информации о выбранных студентах
				$this->updatePupilLessons($new_lesson_id, $pupils);
			}
			exit;
		}
	}

	public function updatePupilLessons($lesson_id, $newPupilsIdArray) {
		//	Удаление всех учеников до изменения
		$pupilsLessons = $this->getElementsFromModel('all', 'PupilLesson', array('lesson_id' => $lesson_id));
		$pupilsLessons = $this->redunantModelName($pupilsLessons, 'PupilLesson');
		foreach ($pupilsLessons as $pupil_lesson) {
			$this->deleteElementFromModel($pupil_lesson['id'], 'PupilLesson');
		}

		//	Сохранение выбранных сейчас учеников
		if ($newPupilsIdArray && !empty($newPupilsIdArray))
			foreach ($newPupilsIdArray as $pupil_id) {
				$this->createElementToModel(array('pupil_id' => $pupil_id, 'lesson_id' => $lesson_id), 'PupilLesson');
			}
	}

	public function lesson_add($trainer_id, $direction_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$pupils = $this->getElementsFromModel('list', 'Pupil', array('direction_id' => $direction_id, 'status' => 1));
		$form_data = array('pupils' => $pupils);
		$this->set('form_data', $form_data);
	}

	public function lesson_edit($trainer_id, $direction_id, $lesson_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$pupils = $this->getElementsFromModel('list', 'Pupil', array('direction_id' => $direction_id, 'status' => 1));
		$lesson = $this->getElementsFromModel('first', 'Lesson', array('id' => $lesson_id));

		//	Создание массива отмеченных учеников
		$sel_array = array();
		$selected = $lesson['Pupil'];
		foreach ($selected as $value) {
			array_push($sel_array, $value['id']);
		}
		//	--- готово

		$lesson_data = array('date' => $lesson['Lesson']['date'], 'id' => $lesson['Lesson']['id'], 'pupils' => $pupils, 'selected' => $sel_array);
		$this->set('form_data', $lesson_data);
	}

	public function lesson_delete($trainer_id, $direction_id, $lesson_id) {
		$this->deleteElementFromModel($lesson_id, 'Lesson');
		//	Удаление ссылок на учеников, посетивших занятие
		$this->updatePupilLessons($lesson_id, null);
		$this->redirect('lesson_list/'.$trainer_id."/".$direction_id);
	}

	public function direction_chart($trainer_id, $direction_id) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer_name', $trainer['User']['name']);

		$chart_data = $this->get_direction_chart_data($direction_id);
		$this->set('chart_data', $chart_data);
	}

	public function direction_statistics($trainer_id, $direction_id, $back_button_action) {
		$this->set('trainer_id', $trainer_id);
		$this->set('direction_id', $direction_id);

		$direction = $this->getElementsFromModel('first', 'Direction', array('id' => $direction_id));
		$this->set('direction_name', $direction['Direction']['name']);

		$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id));
		$this->set('trainer_name', $trainer['User']['name']);

		$this->get_direction_statistics_form($direction_id);

		if ($back_button_action == "stat") {
			$this->set('back_button_action', 'statistics_trainer/'.$trainer_id);
		} else {
			$this->set('back_button_action', 'lesson_list/'.$trainer_id.'/'.$direction_id);
		}
	}

	//	Вывод новых данных при смене даты
	public function get_direction_statistics_form($direction_id) {

		$minYear = $this->getElementsFromModel('first', 'Lesson', array('direction_id' => $direction_id), array('date'), array('order' => array('date')));
		if ($minYear && !empty($minYear)){
			$minYear = $minYear['Lesson']['date'];
			$datetime = new DateTime($minYear);
			$minYear = $datetime->format('Y');
		} else {
			unset($minYear);
		}

		//	получаем данные, если это ajax-запрос
		if( $this->request->is('ajax') ) {

			$this->layout = 'ajax';
			$year = $this->request->data['year'];
			$month = $this->request->data['month'];
			$date = $this->getDateFromTo($year, $month);
			$date_from = $date['date_from'];
			$date_to = $date['date_to'];

		} else {

			$datetime = new DateTime();
			$date = $this->getDateFromTo($datetime->format('Y'), $datetime->format('m'));
			$date_from = $date['date_from'];
			$date_to = $date['date_to'];

		}

		$last_payment = $this->getElementsFromModel('first', 'Payment', array('date <=' => $date_to), null, array('order' => array('id DESC')));
		$visits_data = $this->getVisitsCount($direction_id, $date_from, $date_to);

		//	Подсчет прибыли
		if (!empty($last_payment)) {

			$last_payment = $last_payment['Payment'];

			$n = $visits_data['pupils_count'];

			$k = $last_payment['cost_lesson'];
			$z = $last_payment['cost_trainer'];
			$A = $last_payment['cost_rent'];
			$Q = $last_payment['cost_electricity'];

			if (is_numeric($k) && is_numeric($z) && is_numeric($A) && is_numeric($Q)) {

				$result = $n * $k - $A - $Q - $n * $z;
				$income_money_html = $this->getStatisticsResult($result);

			} else {
				$income_money_html = self::ERR_FIELDS_NUM;
			}
		} else {
			$income_money_html = "N/A";
		}

		$this->set('form_data', $visits_data + array(
			'date' => array(
				'maxYear' => date('Y'),
				'minYear' => @$minYear,
				'value' => date('Y-M-d')
			),
			'cost_lesson' => @$k,
			'cost_trainer' => @$z,
			'cost_rent' => @$A,
			'cost_electricity' => @$Q,
			'income_money_html' => @$income_money_html
		));
	}

	/*
		Получение промежутка дат с текущего месяца до начала следующего
		@params
			$year, $month - дата начала
		@return
			array('date_from' => 'YYYY-MM-01', 'date_to' => 'YYYY-MM-01')
	*/
	private function getDateFromTo($year, $month) {
		$date_from = $year . "-" . $month . "-01";
		if ($month != 12)
			$date_to = $year . "-" . ($month + 1) . "-01";
		else
			$date_to = ($year + 1) . "-01-01";

		return array('date_from' => $date_from, 'date_to' => $date_to);
	}

	private function getVisitsCount($direction_id, $date_from = null, $date_to = null) {
		$chart_data = $this->get_direction_chart_data($direction_id, $date_from, $date_to);
		$lessons_count = count($chart_data);
		$pupils_count = 0;
		foreach ($chart_data as $key => $value) {
			$pupils_count += $value['count'];
		}
		return array('lessons_count' => $lessons_count, 'pupils_count' => $pupils_count);
	}

	public function ajax_statistics_data_check() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$message = "Something gone wrong..";

		$k = $this->request->data['k'];
		$z = $this->request->data['z'];
		$A = $this->request->data['A'];
		$Q = $this->request->data['Q'];
		$n = $this->request->data['n'];

		if (!empty($k) && !empty($z) && !empty($A) && !empty($Q)) {
			if (is_numeric($k) && is_numeric($z) && is_numeric($A) && is_numeric($Q)) {

				$result = $n * $k - $A - $Q - $n * $z;
				$message = $this->formatStatisticsResult("Прибыль: ", "black") . $this->getStatisticsResult($result);

			} else {
				$message = self::ERR_FIELDS_NUM;
			}
		} else {
			$message = self::ERR_FIELDS;
		}

		$this->set('error_message', $message);
	}

	private function formatStatisticsResult($result, $color) {
		return '<span style="color: '.$color.'; ">'.$result.'</span>';
	}

	private function getStatisticsResult($result) {
		if ($result > 0) {
			return $this->formatStatisticsResult($result, "#0ED227");
		} else {
			return $this->formatStatisticsResult($result, "red");
		}
	}

	public function schedule() {
		//	вызов отображения расписания из родительского класса UsersController
		parent::schedule();
	}

	public function uploadSchedule() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->autoRender = false;

		$schedule = $this->getElementsFromModel('first', 'Schedule');

		if (empty($_FILES)) {
			//	Отображение загруженного файла

			if ($schedule && !empty($schedule['Schedule']['image_url'])) {
				$result  = array();
				$obj['name'] = $schedule['Schedule']['image_url'];
				$obj['size'] = filesize(WWW_ROOT . 'files' . DS . 'schedules' . DS . $schedule['Schedule']['image_url']);
				$result[] = $obj;
				header('Content-type: text/json'); 
				header('Content-type: schedule/json');		 
				echo json_encode($result);
			}
		} else {
			//	Загрузка файла
			
			//	Загрузка на сервер
			$this->createScheduleFolder();
			$filelink = WWW_ROOT . 'files' . DS . 'schedules' . DS . $_FILES['file']['name'];
			$result = move_uploaded_file($_FILES['file']['tmp_name'], $filelink);

			//	Загрузка в БД
			if (!$schedule) {
				$schedule = $this->createElementToModel(array(), 'Schedule');
				$schedule = $this->getElementsFromModel('first', 'Schedule');
			}
			$this->updateElementToModel($schedule['Schedule']['id'], array('image_url' => $_FILES['file']['name']), 'Schedule');
		}
	}

	public function deleteSchedule() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}
		$this->autoRender = false;

		//	Удаление из БД
		$schedule = $this->getElementsFromModel('first', 'Schedule');
		$this->updateElementToModel($schedule['Schedule']['id'], array('image_url' => null), 'Schedule');

		//	Удаление с сервера
		if (!unlink(WWW_ROOT . 'files' . DS . 'schedules' . DS . $schedule['Schedule']['image_url'])) {
			$this->log(WWW_ROOT . 'files' . DS . 'schedules' . DS . $schedule['Schedule']['image_url']);
		}	
	}

	public function getScheduleView() {
		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';
		//	вызов отображения расписания из родительского класса UsersController
		parent::schedule();
	}

	private function createScheduleFolder() {
		if (!file_exists(WWW_ROOT . 'files')) {
			mkdir(WWW_ROOT . 'files', 0777, true);
		} 
		if (!file_exists(WWW_ROOT . 'files' . DS . 'schedules')) {
			mkdir(WWW_ROOT . 'files' . DS . 'schedules', 0777, true);
		} 
	}

	public function statistics() {
		$data = $this->request->data;
		if(empty($data)) {
			$trainers = $this->getElementsFromModel('list', 'User', array('type' => 1));
			$this->set('trainers', $trainers);
		} else {
			$trainer_id = $data['Trainer'];
			$this->redirect(array('action' => 'statistics_trainer/' . $trainer_id));
		}
	}

	public function statistics_trainer($trainer_id) {
		$data = $this->request->data;
		if (empty($data)) {
			$trainer = $this->getElementsFromModel('first', 'User', array('id' => $trainer_id), array('id', 'name'));
			$this->set('trainer', $trainer);

			$directions = $this->getElementsFromModel('list', 'Direction', array('user_id' => $trainer_id));
			$this->set('directions', $directions);

			$this->get_ajax_trainer_statistics($trainer_id);

		} else {
			$direction_id = $data['Direction'];
			$this->redirect(array('action' => 'direction_statistics/' . $trainer_id . '/' . $direction_id . '/' . 'stat'));
		}
	}

	public function get_ajax_trainer_statistics($trainer_id) {

		//	Получем самую раннюю дату
		$minYear = $this->getElementsFromModel('first', 'Lesson', array(), array('date'), array('order' => array('date')));
		if ($minYear && !empty($minYear)){
			$minYear = $minYear['Lesson']['date'];
			$datetime = new DateTime($minYear);
			$minYear = $datetime->format('Y');
		} else {
			unset($minYear);
		}

		//	получаем данные, если это ajax-запрос
		if( $this->request->is('ajax') ) {

			$this->layout = 'ajax';
			$year = $this->request->data['year'];
			$month = $this->request->data['month'];
			$date = $this->getDateFromTo($year, $month);
			$date_from = $date['date_from'];
			$date_to = $date['date_to'];

		} else {

			$datetime = new DateTime();
			$date = $this->getDateFromTo($datetime->format('Y'), $datetime->format('m'));
			$date_from = $date['date_from'];
			$date_to = $date['date_to'];

		}

		$visits_chart_data = $this->get_directions_chart_data_visits($trainer_id, $date_from, $date_to);
		$salary_charts_data = $this->get_trainer_salaries($trainer_id, $date_from, $date_to);
		$charts_data = array_merge(array($visits_chart_data), $salary_charts_data);
		
		$this->set('charts_data', $charts_data);

		$this->set('form_data', array(
			'date' => array(
				'maxYear' => date('Y'),
				'minYear' => @$minYear,
				'value' => date('Y-M-d')
			)
		));

	}

	public function payment() {
		$datetime = new DateTime();
		$last_payment = $this->getElementsFromModel('first', 'Payment', array('date <=' => $datetime->format('Y-m-d')), null, array('order' => array('id DESC')));
		$this->set('last_payment', @$last_payment['Payment']);
	}

	public function save_payment() {

		$this->autoRender = false;
		$data = $this->request->data;
		if ($data) {

			//	Получение промежутка дат текущего месяца
			$datetime = new DateTime();
			$date = $this->getDateFromTo($datetime->format('Y'), $datetime->format('m'));

			$save_data = array(
				'cost_lesson' => $data['cost_lesson'],
				'cost_trainer' => $data['cost_trainer'],
				'cost_rent' => $data['cost_rent'],
				'cost_electricity' => $data['cost_electricity'],
				'date' => $datetime->format('Y-m-d')
			);

			//	Получение записи платежей текущего месяца
			$current_payment = $this->getElementsFromModel('first', 'Payment',
				array('and' => array('date >=' => $date['date_from'], 'date <' => $date['date_to']))
			);

			if (!empty($current_payment)) {
				$this->updateElementToModel($current_payment['Payment']['id'], $save_data, 'Payment');
			} else {
				$this->createElementToModel($save_data, 'Payment');
			}

			$this->redirect('payment');
		} else {
			$this->redirectToRoot();
		}
	}

	public function get_trainer_salaries($trainer_id, $date_from = null, $date_to = null) {

		$directions = $this->getElementsFromModel('all', 'Direction', array('user_id' => $trainer_id));
		$directions = $this->redunantModelName($directions, 'Direction');

		//	Настройки поиска общие
		if (($date_from != null) and ($date_to != null)) {
			$condition = array(
				'and' => array(
					'date >=' => $date_from,
					'date <' => $date_to,
				)
			);
		} else {
			$condition = array();
		}
		
		$answer = array();

		if (!empty($directions)) {
			foreach($directions as $direction) {

				//	Настройки поиска под определенное направление
				$condition['direction_id'] = $direction['id'];

				//	Получение всех занятий этого направления
				$lessons = $this->getElementsFromModel(
					'all',
					'Lesson',
					$condition
				);

				//	Количество посещений этого направления
				$visits_count = 0;
				foreach ($lessons as $value) {
					$visits_count += count($value['Pupil']);
				}

				//	Получение самой актуальной на тот день цены
				$last_payment = $this->getElementsFromModel('first', 'Payment', array('date <' => $date_to), null, array('order' => array('id DESC')));

				$salary = ($visits_count === 0) ? 0 : 'N/A';
				if ($last_payment && !empty($last_payment)) {
					$salary = $visits_count * $last_payment['Payment']['cost_trainer'];
				}

				array_push($answer, array('name' => 'Зарплата за "' . $direction['name'] . '"', 'value' => $salary, 'type' => 'single_text'));
			}
		}

		return $answer;

	}
}