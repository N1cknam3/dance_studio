<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	public $helpers = array('Html', 'Session', 'Widgets');
	
	const ERR_FIELDS = "Все поля должны быть заполнены!";
	const ERR_FIELDS_NUM = "Поля должны содержать числовые данные!";
	const ERR_PASS = "Неправильный логин и/или пароль!";
	const ERR_EMAIL = "Введите корректный e-mail!";
	const ERR_USER_EXISTS = "Такой e-mail уже зарегистрирован!";

	protected function login($userID, $type) {
		$this->Session->write('user', $userID);
		$this->Session->write('type', $type);
		// $this->redirectToRoot();	//	Отключил из-за того, что внутри AJAX-запросов нельзя вызывать перенаправления
	}

	protected function isLogin() {
		return ($this->Session->read('user') != "");
	}

	protected function isLoginType($type) {
		return ($this->Session->read('type') == $type);
	}

	protected function getUser() {
		$user_id = $this->Session->read('user');
		return $this->getElementsFromModel('first', 'User', array('id' => $user_id));
	}

	public function logout() {
		$this->Session->write('user', '');
		$this->Session->write('type', '');
		$this->redirectToRoot();
	}

	public function beforeRender() {
		if ($this->isLogin()) {
			if ($this->isLoginType(0)) {
				$this->redirect(array('controller' => 'directors', 'action' => 'cabinet'));
			} else if ($this->isLoginType(1)) {
				$this->redirect(array('controller' => 'trainers', 'action' => 'cabinet'));
			} else {
				$this->logout();
			}
		} else {
			$this->layout = 'users-cabinet';
		}
	}

	public function main() {
		$this->set("title_for_layout", "Танцевальная студия");
	}

	public function ajax_login_check() {
		/*
			AJAX-запрос обрабатывает результаты работы этой ф-ии:
				если данные пришли, значит была ошибка -> вывод сообщения $message,
				иначе - авторизация прошла успешно -> перенаправление в кабинет пользователя
		*/

		if( !$this->request->is('ajax') ) {
			$this->ajaxMethodException();
		}

		$this->layout = 'ajax';

		$email = $this->request->data['User']['email'];
		$password = $this->request->data['User']['password'];

		$user = $this->getElementsFromModel('first', 'User', array('email' => $email, 'password' => $password));
		if (!empty($user)) {
			$this->login($user['User']['id'], $user['User']['type']);
			exit;
		} else {
			$this->set('message', self::ERR_PASS);
			// throw new UnauthorizedException('Check login and/or password');
		}
	}

	public function schedule() {
		$schedule = $this->getElementsFromModel('first', 'Schedule');
		if ($schedule) {
			$this->set("schedule", $schedule['Schedule']['image_url']);
		}
	}

	public function video() {
		$videos = $this->getElementsFromModel('all', 'Video', null, null, array('order' => array('id DESC')));
		$this->set('videos', $videos);
		//	Запрещаем показ в виде кнопок удаления видео
		$this->set('video_delete', false);
	}

}